package Interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.Layer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import Logica.Controller;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.event.ActionEvent;

public class Registracion extends JFrame {

	private JPanel contentPane;
	private Controller cont;
	private JMapViewer map;
	private MapMarkerDot dot;
	//campos
	private JTextField tf_nombre;
	private JTextField tf_edad;
	private JComboBox<String> cb_nacionalidad;	
	private Double posLat;
	private Double posLon;
	
	public Registracion(int posx, int posy, Controller cont) {
		this.cont = cont;
		iniciarVentana(posx, posy);
		agregarCampos();
		agregarBotones();
		agregarMapa();
	}
	private void iniciarVentana(int posx, int posy) {
		setTitle("MD Agentes");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(posx, posy, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
	}
	private void agregarCampos() {
		JLabel lblNewLabel_1 = new JLabel("Agente ");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblNewLabel_1.setBounds(186, 27, 368, 49);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(40, 113, 127, 14);
		contentPane.add(lblNewLabel);
		
		tf_nombre = new JTextField();
		tf_nombre.setName("Nombre");
		tf_nombre.setBounds(177, 110, 153, 20);
		contentPane.add(tf_nombre);
		tf_nombre.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setBounds(40, 169, 46, 14);
		contentPane.add(lblEdad);
		
		tf_edad = new JTextField();
		tf_edad.setName("Edad");
		tf_edad.setBounds(177, 166, 153, 20);
		contentPane.add(tf_edad);
		tf_edad.setColumns(10);
		
		cb_nacionalidad = new JComboBox();
		cb_nacionalidad.addItem("Argentina");
		cb_nacionalidad.addItem("Espa�ol");
		cb_nacionalidad.setBounds(560, 110, 153, 20);
		contentPane.add(cb_nacionalidad);
		
		JLabel lblNacionalidad = new JLabel("Nacionalidad");
		lblNacionalidad.setBounds(404, 113, 127, 14);
		contentPane.add(lblNacionalidad);
		setVisible(true);
		
	}
	private void agregarBotones() {
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionVolver();
			}
		});
		btnVolver.setBounds(23, 537, 89, 23);
		contentPane.add(btnVolver);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validarCampos()) {
					accionRegistrar();
				}
			}
		});
		btnRegistrar.setBounds(122, 537, 89, 23);
		contentPane.add(btnRegistrar);
		
	}
	private void agregarMapa() {
		JLabel lblSeleccionarUbicacion = new JLabel("Seleccionar ubicacion");
		lblSeleccionarUbicacion.setBounds(23, 223, 216, 38);
		contentPane.add(lblSeleccionarUbicacion);
		
		map = new JMapViewer();
		map.setVisible(true);
		map.setZoom(2);
		map.repaint();
		map.setBounds(10, 272, 774, 254);
		
		DefaultMapController dmc = new DefaultMapController(map) { 
			@Override
			public void mouseClicked(MouseEvent e) {
				map.removeAllMapMarkers();
				map.addMapMarker(agregarPunto(e));
				asignarCooredenadas(e);
				
			}			
		};
		contentPane.add(map);		
	}
	private void asignarCooredenadas(MouseEvent e) {
		posLat = map.getPosition(e.getPoint()).getLat();
		posLon = map.getPosition(e.getPoint()).getLon();
	}
	private MapMarkerDot agregarPunto(MouseEvent e) {
		dot = new MapMarkerDot((Coordinate) map.getPosition(e.getPoint()));
		dot.setLayer(new Layer("Agente"));
		dot.setName("Agente");
		dot.setBackColor(new Color(27226));
		dot.getCoordinate();
		
		return dot;
	}
	private void accionVolver() {
		Rectangle rectVentana = getBounds();
		Principal vj = new Principal(rectVentana.x, rectVentana.y, cont);
		vj.setVisible(true);
		dispose();
	}
	private void accionRegistrar() {
		registrarNuevoAgente();
		accionVolver();
	}
	private void registrarNuevoAgente() {
		cont.agregarAgente(tf_nombre.getText(), Integer.parseInt(tf_edad.getText()),  posLon, posLat, (String)cb_nacionalidad.getSelectedItem());
	}
	private boolean validarCampos() {
		boolean ret = true;
		String mensajeFinal = "";
		System.out.println(tf_nombre.getText());
		mensajeFinal += cont.validarNombreAgente(tf_nombre.getText(),tf_nombre.getName());
		mensajeFinal += cont.validarEdadAgente(tf_edad.getText(),tf_edad.getName());
		mensajeFinal += cont.validarCoordenadaAgente(posLon, posLat);
		
		if(!mensajeFinal.isEmpty()) {
			JOptionPane.showMessageDialog(null, mensajeFinal );
			ret = false;
		}
		return ret;
	}
}
