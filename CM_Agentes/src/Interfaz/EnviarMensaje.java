package Interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logica.Agente;
import Logica.Controller;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class EnviarMensaje extends JFrame {

	private JPanel contentPane;
	private Controller cont;
	
	private JComboBox<String> cb_Emisor;
	private JComboBox<String> cb_Receptor;
	private JLabel lblAgenteEmisor;
	private JLabel lblAgenteReceptor;

	public EnviarMensaje(int posx, int posy, Controller controller) {
		this.cont = controller;
		iniciarVentana(posx, posy);
		agregarBotones();
		iniciarCampos();
		}
	private void iniciarCampos() {
		cb_Emisor = new JComboBox();
		cb_Receptor = new JComboBox();
		for(Agente ag : cont.getListaAgentes()) {
			cb_Emisor.addItem(Integer.toString(ag.getId()));
			cb_Receptor.addItem(Integer.toString(ag.getId()));
		}
		cb_Emisor.setBounds(191, 27, 153, 20);
		cb_Receptor.setBounds(191, 64, 153, 20);
		contentPane.add(cb_Emisor);
		contentPane.add(cb_Receptor);
		
		lblAgenteEmisor = new JLabel("Agente Emisor");
		lblAgenteEmisor.setBounds(10, 30, 140, 14);
		contentPane.add(lblAgenteEmisor);
		
		lblAgenteReceptor = new JLabel("Agente Receptor");
		lblAgenteReceptor.setBounds(10, 67, 160, 14);
		contentPane.add(lblAgenteReceptor);
		
	}
	private void iniciarVentana(int posx, int posy) {
		setTitle("MD Agentes");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(posx, posy, 378, 178);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
	}
	private void agregarBotones() {
		JButton btn_Volver = new JButton("Volver");
		btn_Volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionVolver();
			}
		});
		btn_Volver.setBounds(86, 115, 89, 23);
		contentPane.add(btn_Volver);
		
		JButton btn_EnviarMensaje = new JButton("Enviar Mensaje");
		btn_EnviarMensaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(esEmisorReceptorValido()) {
					accionEnviarMensajer();
				}
			}
		});
		btn_EnviarMensaje.setBounds(185, 115, 159, 23);
		contentPane.add(btn_EnviarMensaje);
		
	}
	private void accionVolver() {
		Rectangle rectVentana = getBounds();
		Principal vj = new Principal(rectVentana.x, rectVentana.y, cont);
		vj.setVisible(true);
		dispose();
	}
	private void accionEnviarMensajer() {
		Agente emisor = cont.getListaAgentes().get(Integer.parseInt((String)cb_Emisor.getSelectedItem()));
		Agente receptor = cont.getListaAgentes().get(Integer.parseInt((String)cb_Receptor.getSelectedItem()));
		Rectangle rectVentana = getBounds();
		Principal vj = new Principal(rectVentana.x, rectVentana.y, cont, cont.calcularCaminoAgentesAlEnviarMensaje(emisor, receptor));
		vj.setVisible(true);
		dispose();
	}
	private boolean esEmisorReceptorValido() {
		if(cb_Emisor.getSelectedItem().equals(cb_Receptor.getSelectedItem())) {
			JOptionPane.showMessageDialog(null, "El emisor y receptor deben ser diferentes para enviar un mensaje." );
			return false;
		}
		return true;
	}
}
