package Interfaz;

import java.awt.Color;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.Layer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import Logica.Agente;
import Logica.Controller;
import Logica.GrafoP;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {
	
	private JPanel contentPane;
	private JMapViewer map;
	private Controller cont;
	private LinkedList<MapMarkerDot> dotList;
	
	public Principal() {
		this.cont = new Controller();
		iniciar(100,100);
	}
	public Principal(int posx, int posy, Controller c) {
		this.cont = c;
		iniciar(posx,posy);
	}
	public Principal(int posx, int posy, Controller c, LinkedList<Agente> caminoAgentes) {
		this.cont = c;
		iniciar(posx,posy);
		graficar(caminoAgentes);
	}
	private void iniciar(int posx, int posy) {
		iniciarVentana(posx, posy);
		agregarMapa();
		agregarBotones();
	}
	private void iniciarVentana(int posx, int posy) {
		setTitle("MD Agentes");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(posx, posy, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setVisible(true);
	}
	private void agregarMapa() {
		contentPane.setLayout(null);
		map = new JMapViewer();
		map.setBounds(0, 0, 800, 500);
		contentPane.add(map);
		map.setZoom(2);
		map.setBackground(new Color(99342));
		map.setVisible(true);
		for (Agente a : cont.getListaAgentes()) {
			MapMarkerDot dot = new MapMarkerDot(new Coordinate(a.getPosLat(), a.getPosLon())); // TODO ver si esta bien esto, si esta lat con lat y lon con lon
			dot.setLayer(new Layer("Agente"));
			dot.setName(String.valueOf(a.getId()));
			dot.setBackColor(new Color(27226));
			dot.getCoordinate();
			map.addMapMarker(dot);
		}
	}
	private void agregarBotones() {

		JButton btn_registrar = new JButton("Registrar nuevo agente");
		btn_registrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Rectangle rectVentana = getBounds();
				Registracion vj = new Registracion(rectVentana.x, rectVentana.y, cont);
				vj.setVisible(true);
				dispose();
			}
		});
		contentPane.add(btn_registrar);
		btn_registrar.setBounds(0, 500, 406, 81);
		btn_registrar.setVisible(true);
		
		JButton btn_enviarMensaje = new JButton("Enviar mensaje");
		btn_enviarMensaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cont.disponibleEnviarMensaje()) {
					Rectangle rectVentana = getBounds();
					EnviarMensaje vj = new EnviarMensaje(rectVentana.x, rectVentana.y, cont);
					vj.setVisible(true);
					dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Favor de disponer al menos DOS agentes para enviar el mensaje" );
				}
			}
		});
		contentPane.add(btn_enviarMensaje);
		btn_enviarMensaje.setBounds(404, 500, 396, 71);
		btn_enviarMensaje.setVisible(true);
	}
	private void graficar(LinkedList<Agente> caminoAgentes){
		Agente ultimoAgente = null;
		System.out.println(caminoAgentes);
		for (Agente a : caminoAgentes) {
			if(ultimoAgente != null) {
				Coordinate uno = new Coordinate(ultimoAgente.getPosLat(), ultimoAgente.getPosLon());
				Coordinate dos = new Coordinate(a.getPosLat(), a.getPosLon());
				ArrayList<Coordinate> ruta = new ArrayList<Coordinate>(Arrays.asList(uno, dos, dos));
				map.addMapPolygon(new MapPolygonImpl(ruta));
			}
			ultimoAgente = a;
		}
	}
}
