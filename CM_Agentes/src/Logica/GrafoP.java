package Logica;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.xml.bind.ValidationException;

public class GrafoP {
	
	List<ArrayList<Double>> matriz;

	GrafoP(int vertices){
		if (vertices < 0) 
			throw new IllegalArgumentException("El vertice tiene que ser cero o positivo."); 
		
		matriz = new ArrayList<ArrayList<Double>>();
		
		for (int i = 0; i < vertices; i++) {
			matriz.add(i, new ArrayList<Double>(vertices));
			for (int j = 0; j < vertices; j++) {
				matriz.get(i).add(0.0);
			}
		}
	}
	
	public void aņadirArista(int a, int b, Double dist) {
		if (a >= matriz.size() || b >= matriz.get(0).size() || a < 0 || b < 0) 
			throw new IllegalArgumentException("No se puede agregar una arista a un elemento que no existe en la matriz."); 
		
		matriz.get(a).set(b, dist);
		matriz.get(b).set(a, dist);
		
	}
	
	public void agregarVertice() {
		int sizeAnterior = matriz.size();
		for (int i = 0; i < sizeAnterior+1; i++) {
			if (i >= sizeAnterior) {
				matriz.add(new ArrayList<Double>());
			}
			for (int j = 0; j < sizeAnterior+1; j++) {
				if (i < sizeAnterior && j >= sizeAnterior)
					matriz.get(i).add(0.0);
				if (i >= sizeAnterior)
					matriz.get(i).add(0.0);
			}
		}
	}
	
	public List<ArrayList<Double>> getMatriz(){
		return matriz;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		System.out.println(matriz);
		if(matriz != null || matriz.size()>0 ) {
			for(int x = 0; x < matriz.size(); x++) {
				for(int y = 0; y < matriz.get(0).size(); y++) {
					sb.append("[" + matriz.get(x).get(y) + "]");
				}
				sb.append("\n");
			}
		} else {
			sb.append("[]");
		}
		return sb.toString();
	}
	
	public GrafoP getArbolGeneradorMinimo() { 
		
		GrafoP arbol = new GrafoP (matriz.size());
		
		Double[][] prim = algPrim();
		
		for(int i = 0; i< prim.length; i++) {
			for(int j = 0; j < prim.length;j++) {
				if(prim[i][j] != null) {
					arbol.aņadirArista(i, j, prim[i][j]);
				}
			}
		}
		
		return arbol;
	}
	
	private Double[][] algPrim() {  
        boolean[] marcados = new boolean[matriz.size()]; 
        return algPrim(matriz, marcados, 0, new Double[matriz.size()][matriz.size()]); 
	}
    private Double[][] algPrim(List<ArrayList<Double>> Matriz, boolean[] marcados, int verticeIndex, Double[][] Final) {
        marcados[verticeIndex] = true;
        Double aux = -1.0;
        if (!todosMarcados(marcados)) { 
            for (int i = 0; i < marcados.length; i++) { 
                if (marcados[i]) {
                    for (int j = 0; j < Matriz.size(); j++) {
                        if (Matriz.get(i).get(j) != 0) {        
                            if (!marcados[j]) {
                                if (aux == -1) {        
                                    aux = Matriz.get(i).get(j);
                                } else {
                                    aux = Math.min(aux, Matriz.get(i).get(j));
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < marcados.length; i++) {
                if (marcados[i]) {
                    for (int j = 0; j < Matriz.size(); j++) {
                        if (Matriz.get(i).get(j).equals(aux)) {
                            if (!marcados[j]) { 
                                Final[i][j] = aux; 
                                Final[j][i] = aux;
                                return algPrim(Matriz, marcados, j, Final);
                            }
                        }
                    }
                }
            }
        }
        return Final;
    }
    
    private boolean todosMarcados(boolean[] vertice) { 
        for (boolean b : vertice) {
            if (!b) {
                return b;
            }
        }
        return true;
    }
}
