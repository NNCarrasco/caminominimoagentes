package Logica;

public class Agente {
	private int id;
	private String nombre;
	private int edad;
	private String nacionalidad;
	private Double posLon;
	private Double posLat;
	
	public Agente(int id, String nombre, int edad, Double posLon, Double posLat, String nacionalidad) {
		if (nombre == null || posLon == null || posLat == null || nacionalidad == null)
			throw new IllegalArgumentException("Todos los parametros de entrada necesitan tener ser diferentes de nulo."); 
		
		this.id = id;
		this.nombre = nombre;
		this.edad = edad;
		this.posLon = posLon;
		this.posLat = posLat;
		this.nacionalidad = nacionalidad;
	}
	public Double getPosLat() {
		return posLat;
	}
	public Double getPosLon() {
		return posLon;
	}
	public int getEdad() {
		return edad;
	}
	public String getNombre() {
		return nombre;
	}
	public int getId() {
		return id;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	@Override
	public String toString(){
		return String.valueOf(id);
		
	}
}
