package Logica;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

class GrafoPTest {
	@Test
	void iniciarGrafoNegativo() {
		assertThrows(IllegalArgumentException.class, () -> {
			GrafoP graf = new GrafoP(-12);
	    });
	}
	@Test
	void agregarVertice() {
		LinkedList<ArrayList<Double>> matOriginal = new LinkedList<ArrayList<Double>> ();

		GrafoP graf = new GrafoP(0);
		matOriginal.addAll(graf.getMatriz());
		
		graf.agregarVertice();
		List<ArrayList<Double>> mat = graf.getMatriz();
		
		assertNotEquals(mat.size(), matOriginal.size());
	}
	@Test
	void agregarAristaValorNegativo() {
		GrafoP graf = new GrafoP(0);
		graf.agregarVertice();
		graf.agregarVertice();
		
		assertThrows(IllegalArgumentException.class, () -> {
			graf.aņadirArista(-1, 0, 2.2);
	    });
	}
	@Test
	void agregarAristaValorMayorAlGrafo() {
		GrafoP graf = new GrafoP(0);
		graf.agregarVertice();
		graf.agregarVertice();
		
		assertThrows(IllegalArgumentException.class, () -> {
			graf.aņadirArista(2, 0, 2.2);
	    });
	}
}
