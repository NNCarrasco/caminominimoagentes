package Logica;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Pattern;

public class Controller {
	
	 LinkedList<Agente> listaAgente;
	 GrafoP grafoAgentes;
	 int lastId;
	
	public Controller() {
		iniciarListaDeAgentes();
		iniciarGrafoP();
	}
	
	public LinkedList<Agente> getListaAgentes(){
		return this.listaAgente;
	}
	
	private void iniciarListaDeAgentes() {
		this.listaAgente = new LinkedList<Agente>();
		this.lastId = 0;
	}
	
	private void iniciarGrafoP() {
		this.grafoAgentes = new GrafoP (0);
	}
	
	public boolean disponibleEnviarMensaje() {
		return listaAgente.size() >= 2;
	}
	
	public void agregarAgente( String nombre, int edad, Double posLon, Double posLat, String nacionalidad) {
		if (nombre == null || posLon == null || posLat == null || nacionalidad == null)
			throw new IllegalArgumentException("Todos los parametros de entrada necesitan tener ser diferentes de nulo."); 
		
		Agente nuevo = new Agente(lastId, nombre, edad, posLon, posLat, nacionalidad);
		
		listaAgente.add(nuevo);
		grafoAgentes.agregarVertice();
		lastId++;
		
		if(listaAgente.size() >= 2) {
			for(int i = 0; i<listaAgente.size(); i++) { // No me gusta, se podria hacer mas rapido
				for(int j = 0; j<listaAgente.size(); j++) {
					if(listaAgente.get(i).getId() != listaAgente.get(j).getId()) {
						// TODO ver que se cuese aca
						grafoAgentes.aņadirArista(i, j, calcularDistanciaPorCoordenada(listaAgente.get(i).getPosLat(),listaAgente.get(i).getPosLon(), listaAgente.get(j).getPosLat(),listaAgente.get(j).getPosLon(), "k" ));
					}
				}
			}
		}
	}
	
	private GrafoP calcularCaminoMinimo() {
		return grafoAgentes.getArbolGeneradorMinimo();
	}
	
	private static double calcularDistanciaPorCoordenada(double lat1, double lon1, double lat2, double lon2, String unit) {
		if ((lat1 == lat2) && (lon1 == lon2)) {
			return 0;
		}
		else {
			double theta = lon1 - lon2;
			double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;
			if (unit == "K") {
				dist = dist * 1.609344;
			} else if (unit == "N") {
				dist = dist * 0.8684;
			}
			return dist;
		}
	}

	public LinkedList<Agente> calcularCaminoAgentesAlEnviarMensaje(Agente emisor, Agente receptor){
		ArrayList<ArrayList<Double>> mat = (ArrayList<ArrayList<Double>>) calcularCaminoMinimo().getMatriz();
		
		validarCaminoAgentes(emisor,receptor, mat);
		
		return calcularCaminoAgentesAlEnviarMensaje(emisor.getId(), receptor.getId(), mat);
	}
	
	private void validarCaminoAgentes(Agente emisor, Agente receptor, ArrayList<ArrayList<Double>> mat) {
		try {
			mat.get(emisor.getId()).get(receptor.getId());
		}catch(Exception e){
			throw new IllegalArgumentException("El agente emisor y receptor tiene que ser parte de el controller.");
		}
		
	}

	private LinkedList<Agente> calcularCaminoAgentesAlEnviarMensaje(int a, int b,  ArrayList<ArrayList<Double>> mat) { 
		LinkedList<Agente> caminoAgente = new LinkedList<Agente>();
		
		if(mat.get(a).get(b) == 0) {
			for(int i=0; i<mat.get(0).size(); i++) {
				if(mat.get(a).get(i) != 0) {
					LinkedList<Agente> agentes = calcularCaminoAgentesAlEnviarMensaje(i,b,a,mat);
					if(agentes.size() != 0) {
						caminoAgente.add(listaAgente.get(a));
						caminoAgente.addAll(agentes);
						return caminoAgente;
					}
				}
			}
		} else {
			caminoAgente.add(listaAgente.get(a));
			caminoAgente.add(listaAgente.get(b));
		}
		
		return caminoAgente;
	}
	private LinkedList<Agente> calcularCaminoAgentesAlEnviarMensaje(int a, int b, int lasta,  ArrayList<ArrayList<Double>> mat) {
		LinkedList<Agente> caminoAgente = new LinkedList<Agente>();
		if(mat.get(a).get(b) == 0) {
			for(int i=0; i<mat.get(0).size(); i++) {
				if(mat.get(a).get(i) != 0 && i != lasta) {
					LinkedList<Agente> agentes = calcularCaminoAgentesAlEnviarMensaje(i,b,a,mat);
					if(agentes.size() != 0) {
						caminoAgente.add(listaAgente.get(a));
						caminoAgente.addAll(agentes);
						return caminoAgente;
					}
				}
			}
		} else {
			caminoAgente.add(listaAgente.get(a));
			caminoAgente.add(listaAgente.get(b));
		}
		return caminoAgente;
	}
	
	/*
	 *	VALIDACIONES 
	 */
	
	public String validarNombreAgente(String nombre, String campoNombre) {
		String ret = "";
		int maxChar = 25;
		
		if(campoNombre == null || campoNombre.isEmpty()) {
			ret = "El nombre del campo es obligatorio \n";
		}
		if(nombre == null || nombre.isEmpty()) {
			ret = "El campo " + campoNombre + " es obligatorio \n";
		} else {
			if( nombre.length() > maxChar ) {
				ret += "El campo " + campoNombre + " tiene que ser tener menos caracteres de " + maxChar + "\n";
			}
		}
		return ret;
	}
	public String validarEdadAgente(String edad, String campoNombre) {
		String ret = "";
		int edadMax = 150;
		int edadMin = 1;
		int edadInt;
		
		if(campoNombre == null || campoNombre.isEmpty()) {
			ret = "El nombre del campo es obligatorio \n";
		}
		if(edad == null || edad.isEmpty()) {
			ret += "El campo " + campoNombre + " es obligatorio \n";
		} else {
			if(!Pattern.compile( "^[0-9]*$" ).matcher( edad ).find()) {
				ret += "El campo " + campoNombre + " debe de estar compuesta solo por numeros \n";
			} else {
				edadInt = Integer.parseInt(edad);
				if(edadInt < edadMin || edadInt > edadMax) {
					ret += "El campo " + campoNombre + " debe estar en el rango " + edadMin +" - "+ edadMax + "\n";	
				} 
			}
		}
		return ret;
	}
	public String validarCoordenadaAgente(Double posLon, Double posLat) {
		String ret = "";
		if(posLon == null || posLat == null) {
			ret = "Ubicar un agente es obligatorio \n";
		}
		return ret;
	}
}
