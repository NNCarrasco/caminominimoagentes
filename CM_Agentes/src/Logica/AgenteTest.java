package Logica;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class AgenteTest {
	@Test 
	void agenteNombreNulo() {
		assertThrows(IllegalArgumentException.class, () -> {
			Agente a = new Agente(1, null, 2, 2.0, 2.0, "Argentina");
	    });
	}
	@Test 
	void agentePosLongitudNulo() {
		assertThrows(IllegalArgumentException.class, () -> {
			Agente a = new Agente(1, "test", 2, null, 2.0, "Argentina");
	    });
	}
	@Test 
	void agentePosLatitudNulo() {
		assertThrows(IllegalArgumentException.class, () -> {
			Agente a = new Agente(1, "test", 2, 2.0, null, "Argentina");
	    });
	}
	@Test 
	void agenteNacionalidadNulo() {
		assertThrows(IllegalArgumentException.class, () -> {
			Agente a = new Agente(1, "test", 2, 2.0, 2.0, null);
	    });
	}
	@Test 
	void agente() {
		Agente agente = new Agente(1, "test", 2, 2.0, 2.0, "Argentina");
		boolean validacion = validacionAgente(agente, "test", 2.0, 2.0, "Argentina");
		assertTrue(validacion);
	}
	private static boolean validacionAgente(Agente a, String nombre, Double posLon, Double posLat, String nacionalidad) {
		if( !a.getNombre().equals(nombre) )
			return false;
		if( !a.getPosLon().equals(posLon) )
			return false;
		if( !a.getPosLat().equals(posLat) )
			return false;
		if( !a.getNacionalidad().equals(nacionalidad) )
			return false;
		return true;
	}
}
