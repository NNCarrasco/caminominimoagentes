package Logica;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

public class ControllerTest {
	@Test
	void agregarAgente() {
		Controller cont = new Controller();
		cont.agregarAgente("Test", 2, 2.0, 20.0, "Argentina");
		assertTrue(cont.getListaAgentes().size() == 1);
	}
	@Test 
	void agregarAgenteNombreNulo() {
		Controller cont = new Controller();
		assertThrows(IllegalArgumentException.class, () -> {
			cont.agregarAgente( null, 2, 2.0, 2.0, "Argentina");
	    });
	}
	@Test 
	void agregarAgentePosLongitudNulo() {
		Controller cont = new Controller();
		assertThrows(IllegalArgumentException.class, () -> {
			cont.agregarAgente( "test", 2, null, 2.0, "Argentina");
	    });
	}
	@Test 
	void agregarAgentePosLatitudNulo() {
		Controller cont = new Controller();
		assertThrows(IllegalArgumentException.class, () -> {
			cont.agregarAgente( "test", 2, 2.0, null, "Argentina");
	    });
	}
	@Test 
	void agregarAgenteNacionalidadNulo() {
		Controller cont = new Controller();
		assertThrows(IllegalArgumentException.class, () -> {
			cont.agregarAgente( "test", 2, 2.0, 2.0, null);
	    });
	}
	@Test 
	void calcularCaminoMinimo() {
		Controller cont = new Controller();
		cont.agregarAgente("Test", 2, 2.0, 20.0, "Argentina");
		cont.agregarAgente("Test", 2, 2.1, 21.0, "Argentina");
		cont.agregarAgente("Test", 2, 3.1, 31.0, "Argentina");
		LinkedList<Agente> agentes = cont.getListaAgentes();
		LinkedList<Agente> caminoAgentes = cont.calcularCaminoAgentesAlEnviarMensaje(agentes.get(0), agentes.get(1));
		assertEquals(2, caminoAgentes.size());
	}
	
	/*
	 * Test de validaciones
	 */
	
	@Test
	void validarNombreAgente() {
		Controller cont = new Controller();
		String respuesta = cont.validarNombreAgente("Test", "Campo Test");
		assertEquals("", respuesta);
	}
	@Test
	void validarNombreAgenteNombreNulo() {
		Controller cont = new Controller();
		String respuesta = cont.validarNombreAgente(null, "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarNombreAgenteNombreVacio() {
		Controller cont = new Controller();
		String respuesta = cont.validarNombreAgente("", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarNombreAgenteCampoNulo() {
		Controller cont = new Controller();
		String respuesta = cont.validarNombreAgente("Test", null);
		assertNotEquals("", respuesta);
	}
	@Test
	void validarNombreAgenteCampoVacio() {
		Controller cont = new Controller();
		String respuesta = cont.validarNombreAgente("Test", "");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarNombreAgenteMuyLargo() {
		Controller cont = new Controller();
		String respuesta = cont.validarNombreAgente("Lorem ipsum dolor sit amet consectetur adipicing elit netus", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgente() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("34", "Campo Test");
		assertEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadVacio() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadNulo() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente(null, "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteCampoVacio() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("34", "");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteCampoNulo() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("34", null);
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadConEspacios() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("3 4", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadConLetras() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("Treinta y Cuatro", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadCero() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("0", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadNegativa() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("-12", "Campo Test");
		assertNotEquals("", respuesta);
	}
	@Test
	void validarEdadAgenteEdadMayorA150() {
		Controller cont = new Controller();
		String respuesta = cont.validarEdadAgente("151", "Campo Test");
		assertNotEquals("", respuesta);
	}	
	@Test
	void validarCoordenadaAgente() {
		Controller cont = new Controller();
		String respuesta = cont.validarCoordenadaAgente(2.0, 2.0);
		assertEquals("", respuesta);
	}	
	@Test
	void validarCoordenadaAgenteLatitudNulo() {
		Controller cont = new Controller();
		String respuesta = cont.validarCoordenadaAgente(2.0, null);
		assertNotEquals("", respuesta);
	}
	@Test
	void validarCoordenadaAgenteLongitudNulo() {
		Controller cont = new Controller();
		String respuesta = cont.validarCoordenadaAgente(null, 2.0);
		assertNotEquals("", respuesta);
	}
}
